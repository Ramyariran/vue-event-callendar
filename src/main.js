import Vue from 'vue'
import Buefy from 'buefy'
import VModal from 'vue-js-modal'
import VueSweetalert2 from 'vue-sweetalert2'

import App from './App.vue'
import store from './store'

import './registerServiceWorker'

import 'buefy/lib/buefy.css'

Vue.config.productionTip = false

Vue.use(Buefy)
Vue.use(VModal)
Vue.use(VueSweetalert2)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
