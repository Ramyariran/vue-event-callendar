import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    events: []
  },
  mutations: {
    addEvent (state, event) {
      state.events = [...state.events, event]
    },
    updateEvent (state, data) {
      let events = state.events.filter(event => event.id !== data.oldID)
      state.events = [...events, data.new]
    },
    removeEvent (state, event) {
      let events = state.events
      events.splice(events.indexOf(event), 1)
    }
  }

})
